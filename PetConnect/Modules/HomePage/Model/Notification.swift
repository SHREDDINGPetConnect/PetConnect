//
//  Notification.swift
//  PetConnect
//
//  Created by Алёна Максимова on 20.08.2023.
//

import Foundation

struct Notification {
    var name: String
    
}

class NotificationPlaceholder {
    static var notifications: [Notification] = []
    
}
