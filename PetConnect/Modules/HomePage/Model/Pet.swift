//
//  Pet.swift
//  PetConnect
//
//  Created by Andrey on 13.08.2023.
//

import UIKit

struct Pet {
    let name: String
    let photo: UIImage?
}
